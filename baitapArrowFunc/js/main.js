const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];
let renderColors = () => {
  let contentHTML = "";

  for (let index = 0; index < colorList.length; index++) {
    let contentColor = `<button class="color-button ${colorList[index]}" onClick="changeColorBtn('${colorList[index]}',${index})" ></button>`;
    contentHTML += contentColor;
  }
  document.getElementById("colorContainer").innerHTML = contentHTML;
};
renderColors();

let colorBefore = colorList[0];
const house = document.getElementById("house");
const changeColorBtn = (colorHouse) => {
  house.classList.remove(colorBefore);
  house.classList.add(colorHouse);
  colorBefore = colorHouse;
};
