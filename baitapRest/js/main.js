class student {
  constructor(...other) {
    return other;
  }
}

tinhDiemTB = (array) => {
  let diemTB = 0;
  let Tong = 0;
  arr.forEach((item) => {
    Tong += item;
  });
  diemTB = Tong / arr.length;
  return diemTB;
};

document.getElementById("btnKhoi1").addEventListener("click", () => {
  let toan = document.getElementById("inpToan").value * 1;
  let ly = document.getElementById("inpLy").value * 1;
  let hoa = document.getElementById("inpHoa").value * 1;

  if (hoa != 0 && toan != 0 && ly != 0) {
    let diemTBKhoi11 = new student(toan, ly, hoa);
    let diemTB = tinhDiemTB(diemTBKhoi11);
    let diemTBformated = (Math.round(diemTB * 100) / 100).toFixed(2);
    document.getElementById("tbKhoi11").innerHTML = `${diemTBformated}`;
  } else {
    document.getElementById("tbKhoi11").innerHTML = "Err";
  }
});

document.getElementById("btnKhoi2").addEventListener("click", () => {
  let van = document.getElementById("inpVan").value * 1;
  let su = document.getElementById("inpSu").value * 1;
  let dia = document.getElementById("inpDia").value * 1;
  let tienganh = document.getElementById("inpEnglish").value * 1;

  if (van != 0 && su != 0 && dia != 0 && tienganh != 0) {
    let diemTBKhoi12 = new student(van, su, dia, tienganh);
    let diemTB = tinhDiemTB(diemTBKhoi12);
    let diemTBformated = (Math.round(diemTB * 100) / 100).toFixed(2);
    document.getElementById("tbKhoi2").innerHTML = `${diemTBformated}`;
  }
});
